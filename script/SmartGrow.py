import sys
import time
from datetime import datetime

import Adafruit_DHT as dht
import RPi.GPIO as GPIO

sys.path.append('/home/pi/smartgrow')
from gmail_utils import EmailUtils

pin_led = 18
sensor = 22
pin = 17

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(18, GPIO.OUT)

while True:
    humidity, temperature = dht.read_retry(sensor, pin)
    if (humidity is not None) and (temperature is not None):
        nowtime = datetime.strftime(datetime.now(), "%d.%m %H:%M:%S")
        temperature = round(temperature, 1)
        humidity = round(humidity, 1)
        EmailUtils.send_email_notification(temperature, humidity, nowtime)
        print(str(nowtime) + "\t" + str(temperature) + "C\t" + str(humidity) + "%")
        if temperature > 26:
            GPIO.output(18, GPIO.HIGH)
            print('LIGHT IS ON!')
        else:
            GPIO.output(18, GPIO.LOW)

        time.sleep(10)
